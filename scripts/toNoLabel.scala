import java.io._
import scala.collection.JavaConversions._
import scala.io.Source
import java.io.PrintWriter

/***
* ラベルつきのものをラベルなしに。
*/
val base_folder = argv.head
val op = argv.tail.head


val dir = new File(base_folder);

val files = dir.listFiles.filter(x => x.toString.split('.').last == "lab")

def output(path : String, l : List[String]){
	val _b = new PrintWriter(path)
	l.foreach(_b.println)
	_b.close
}

files.foreach(f => {
	val tn = f.toString + ".tmp"
	val _t = Source.fromFile(f.toString).getLines.toList
	val _b = new PrintWriter(tn)
	_t.foreach( k => {
		_b.println(k.split(' ').last)
	})
	_b.close
	//_t.close
	val o = new PrintWriter(f.toString)
	val i = Source.fromFile(tn).getLines.toList
	i.foreach( k => {
		o.println(k)
	})
	o.close
	//i.close
}
)



//ついでにdict,labels出力とmlfの作成を行う
//create mlf

val lab_files = dir.listFiles.filter(x => x.toString.split('.').last == "lab").map(_.toString).toList


//統計情報出したいなら、list?
def createOutput(f : List[String], o : List[String] = Nil,s : Set[String] = Set()) : (List[String],Set[String]) = {
	f match {
		case Nil => (o,s)
		case h :: t => {
			val _f : List[String] = Source.fromFile(h).getLines.toList
			createOutput(t, List("#!MLF!#","\"" + h + "\"") ::: (_f ::: List(".")) ::: o,s ++ _f.toSet)
		}
	}
}

val (mlf,s) = createOutput(lab_files)

//output
output(base_folder + "/" + op + ".mlf",mlf)
output(base_folder + "/labels",s.toList)
output(base_folder + "/dict",s.toList.map(x => { x + " " + x + " sp"})) //add sp
	
