import java.io._
import scala.collection.JavaConversions._
import scala.io.Source
import java.io.PrintWriter

//
val f = argv.head
val dir = new File(f);
val files = dir.listFiles.filter(x => x.toString.split('.').last == "phn")
files.foreach(println)

files.foreach(x => {
	val name = x.toString.split('/').last.split('.').head + ".lab"
	val b = Source.fromFile(x).getLines.toList
	println(name)
	val pw = new PrintWriter(dir.getAbsolutePath.toString + "/" + name)
	pw.print(name + " ")
	b.dropRight(1).foreach(y => pw.print(y + " "))
	pw.println(b.last)
	pw.close
}	
)


