import scala.sys.process.Process
import scala.sys.process.stringToProcess

/**
 * creat datasets
 */

	val train_folder = "datasets/train"
	val scripts_hoder = "scripts"
	  
	//create dict
	//input : wlist,beep 
	//output : monophones1,dict
	//need : global.ded
	
	println("HDMan -m -w config/wlsit -n config/monophones1 -l config/dlog config/dict config/beep" !!)	
	println("output >> config/monophones1,config/dict")

	//create lab files
	//input : train/*.phn
	//output : train/*.lab
	println(s"exec >> scala ${scripts_hoder}/batch/mkLabelFromPhn.scala ${train_folder}")
	println(s"scala ${scripts_hoder}/batch/mkLabelFromPhn.scala ${train_folder}" !!)
	println(s"output >>  ${train_folder}/*.lab")
	
	//create words.mlf
	//input : train/*.lab
	//output : train/words.mlf
	println(s"exec >> scala ${scripts_hoder}/createWordMLF.scala ${train_folder}")
	println(s"scala ${scripts_hoder}/createWordMLF.scala ${train_folder}"  !!)
	println(s"output >>  ${train_folder}/words.mlf")
	
	//create phones0.mlf
	//input : dict, mkphones0.led, words.mlf
	//output : phones0.mlf
	//need : mkphones0.led
	println(s"exec >> HLEd -l '*' -d config/dict -i ${train_folder}/phones0.mlf config/mkphones0.led ${train_folder}/words.mlf")
	println(s"HLEd -l '*' -d config/dict -i ${train_folder}/phones0.mlf config/mkphones0.led ${train_folder}/words.mlf"  !!)
	println(s"output >>  ${train_folder}/phones0.mlf")
	
	//create mvs files(it is binary features file)
	//input : *.mv
	//output : *.mvs, train.scp
	println(s"exec >> python ${scripts_hoder}/toTimitDataFormat.py ${train_folder}")
	println(s"python ${scripts_hoder}/toTimitDataFormat.py ${train_folder}"  !!)
	println(s"output >>  ${train_folder}/*.mvs,scp")

