# coding:utf-8
import sys,csv,glob
from struct import *

argvs = sys.argv  # コマンドライン引数を格納したリストの取得
argc = len(argvs) # 引数の個数


if (argc < 2):   # 引数が足りない場合は、その旨を表示
    print 'Usage: # python %s filename' % argvs[0]
    quit()         # プログラムの終了

#自作なので9番
parmKind = 9

# 設定ファイルの読み込み
f = open(argvs[1] + "/config",'r')
sampPeriod = int(f.readline().rstrip("\n")) # 1行読み込む(改行文字も含まれる)
sampSize = int(f.readline().rstrip("\n"))
f.close

files = glob.glob(argvs[1] + '/*.mv')

mvs =[]
print files
for file in files:
    f = open(file, 'r')
    features = []
    c = csv.reader(f)  # CSV読み込み用オブジェクトの生成
    fil = False
    for row in c:
        if fil:
            features.append(row)
        else:
            fil = True

    nSamples = len(features)
    ffn = file.split("/")
    fn = argvs[1] + "/" + ffn[len(ffn) -1].split(".")[0] + ".mvs"
    print fn
    wb = open(fn, 'wb')
    wb.write(pack('>L', nSamples ))
    wb.write(pack('>L', sampPeriod))
    wb.write(pack('>H', sampSize * 4))
    wb.write(pack('>H', parmKind ))

    for feature in features:
        for ff in feature:
            wb.write(pack('>f', float(ff)))
	    
    mvs.append(fn)
    wb.close
    f.close()
f = open(argvs[1] + "/train.scp","w")
for mv in mvs:
    f.write(mv + "\n")
