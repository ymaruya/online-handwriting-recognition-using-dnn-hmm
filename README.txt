
2次元の特徴量を持つ、オンライン手書き認識 using HTK


---
datasets/
	train/
	test/
	mvs/
	config : フレームシフトと特徴量を記述


---
hmms/
	proto_hmm
	hmm0/

proto_hmm


---
scripts/
	toTimitDataFormat.py : 特徴量をtimit data formatに直す
	

---
TODO : 
1. mlfファイルの用意
 => どっかのデータセットからもってくるのが一番
 http://www.yusukekondo.com/htk.html
sample
#!MLF!#
"n001.lab"
the
north
wind
and
the
sun
were
disputing
.
"n002.lab"
...
 
MKFILEの作成
	
参考 : https://github.com/SnippyHolloW/timit_tools/blob/master/Makefile

$(TMP_TRAIN_FOLDER) = ./hmms
$(dataset_train_folder) = ./datasets

# 特徴量ファイルの作成
python ./scripts/toTimitDataFormat.py  $(dataset_train_folder)


cp $(dataset_train_folder)/labels $(TMP_TRAIN_FOLDER)/monophones0
cp $(dataset_train_folder)/train.mlf $(TMP_TRAIN_FOLDER)/
cp $(dataset_train_folder)/train.mvs $(TMP_TRAIN_FOLDER)/


HCompV -f 0.0001 -m -S ./datasets/mvs/train.mvs -M ./hmms/hmm0 ./hmms/proto.hmm

python src/create_hmmdefs_from_proto.py $(TMP_TRAIN_FOLDER)/hmm_mono_simple0/proto $(TMP_TRAIN_FOLDER)/monophones0 $(TMP_TRAIN_FOLDER)/hmm_mono_simple0/ $(TMP_TRAIN_FOLDER)/hmm_mono_simple0/vFloors