train_monophones_monogauss:
		@echo -e "*** training the HMMs with HTK ***"
		@echo -e "using folder $(dataset_train_folder)"
		@echo -e "\n>>> preparing the HMMs\n"
		cp $(dataset_train_folder)/labels $(TMP_TRAIN_FOLDER)/monophones0
		cp $(dataset_train_folder)/train.mlf $(TMP_TRAIN_FOLDER)/
		cp $(dataset_train_folder)/train.scp $(TMP_TRAIN_FOLDER)/
		python -c "import sys;print '( < ' + ' | '.join([line.strip('\n') for line in sys.stdin]) + ' > )'" < $(TMP_TRAIN_FOLDER)/monophones0 > $(TMP_TRAIN_FOLDER)/gram
		HParse $(TMP_TRAIN_FOLDER)/gram $(TMP_TRAIN_FOLDER)/wdnet
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_mono_simple0
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_mono_simple1
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_mono_simple2
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_mono_simple3
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_final
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_mono_simple_final
		# -A -D -T 1 
		HCompV -f 0.0001 -m -S $(TMP_TRAIN_FOLDER)/train.scp -M $(TMP_TRAIN_FOLDER)/hmm_mono_simple0 $(TMP_TRAIN_FOLDER)/proto.hmm
		python scripts/createHMMsFromProto.py $(TMP_TRAIN_FOLDER)/hmm_mono_simple0/proto $(TMP_TRAIN_FOLDER)/monophones0 $(TMP_TRAIN_FOLDER)/hmm_mono_simple0/ $(TMP_TRAIN_FOLDER)/hmm_mono_simple0/vFloors
		@echo -e "\n>>> training the HMMs (3 times)\n"
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mono_simple0/macros -H $(TMP_TRAIN_FOLDER)/hmm_mono_simple0/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mono_simple1 $(TMP_TRAIN_FOLDER)/monophones0 
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mono_simple1/macros -H $(TMP_TRAIN_FOLDER)/hmm_mono_simple1/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mono_simple2 $(TMP_TRAIN_FOLDER)/monophones0 
		HERest -s $(TMP_TRAIN_FOLDER)/stats -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mono_simple2/macros -H $(TMP_TRAIN_FOLDER)/hmm_mono_simple2/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mono_simple3 $(TMP_TRAIN_FOLDER)/monophones0 
		cp $(TMP_TRAIN_FOLDER)/hmm_mono_simple3/* $(TMP_TRAIN_FOLDER)/hmm_final/
		#for test
		cp $(TMP_TRAIN_FOLDER)/hmm_mono_simple3/* $(TMP_TRAIN_FOLDER)/hmm_mono_simple_final/
		cp $(dataset_train_folder)/dict $(TMP_TRAIN_FOLDER)/dict
		cp $(TMP_TRAIN_FOLDER)/monophones0 $(TMP_TRAIN_FOLDER)/phones
test_monophones:
		@echo -e "*** testing the monophone trained model ***"
		HVite -p 2.5 -s 5.0 -w $(TMP_TRAIN_FOLDER)/wdnet -H $(TMP_TRAIN_FOLDER)/hmm_final/hmmdefs -i $(TMP_TRAIN_FOLDER)/outtrans.mlf -S $(dataset_test_folder)/test.scp -o ST $(TMP_TRAIN_FOLDER)/dict $(TMP_TRAIN_FOLDER)/phones
		#HVite -w $(TMP_TRAIN_FOLDER)/wdnet -H $(TMP_TRAIN_FOLDER)/hmm_final/hmmdefs -i $(TMP_TRAIN_FOLDER)/outtrans.mlf -S $(dataset_test_folder)/test.scp -o ST $(TMP_TRAIN_FOLDER)/dict $(TMP_TRAIN_FOLDER)/phones
		HResults -I $(dataset_test_folder)/test.mlf $(TMP_TRAIN_FOLDER)/phones $(TMP_TRAIN_FOLDER)/outtrans.mlf
add_short_pauses:
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_silence_model
		python scripts/create_short_pause_silence_model.py $(TMP_TRAIN_FOLDER)/hmm_final/hmmdefs $(TMP_TRAIN_FOLDER)/hmm_silence_model/hmmdefs $(TMP_TRAIN_FOLDER)/monophones1
		#tr "\n" " | " < $(TMP_TRAIN_FOLDER)/monophones1 > $(TMP_TRAIN_FOLDER)/gram
		awk '{if(!$$2) print $$1 " " $$1}' $(TMP_TRAIN_FOLDER)/monophones1 | sort > $(TMP_TRAIN_FOLDER)/dict # TODO replace sort by a script because GNU sort sucks
		echo -e "silence sil" >> $(TMP_TRAIN_FOLDER)/dict # why?
tweak_silence_model: add_short_pauses
		#fixture slice model
		@echo -e "\n>>> tweaking the silence model\n"
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_mono_silence0
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_mono_silence1
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_mono_silence2
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_mono_silence3
		cp $(TMP_TRAIN_FOLDER)/hmm_silence_model/hmmdefs $(TMP_TRAIN_FOLDER)/hmm_mono_silence0/hmmdefs
		cp $(TMP_TRAIN_FOLDER)/hmm_silence_model/macros $(TMP_TRAIN_FOLDER)/hmm_mono_silence0/macros
		HHEd -H $(TMP_TRAIN_FOLDER)/hmm_mono_silence0/macros -H $(TMP_TRAIN_FOLDER)/hmm_mono_silence0/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mono_silence1 sil.hed $(TMP_TRAIN_FOLDER)/monophones1
		@echo -e "\n>>> re-training the HMMs\n"
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mono_silence1/macros -H $(TMP_TRAIN_FOLDER)/hmm_mono_silence1/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mono_silence2 $(TMP_TRAIN_FOLDER)/monophones1
		HERest -s $(TMP_TRAIN_FOLDER)/stats -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mono_silence2/macros -H $(TMP_TRAIN_FOLDER)/hmm_mono_silence2/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mono_silence3 $(TMP_TRAIN_FOLDER)/monophones1 
		cp $(TMP_TRAIN_FOLDER)/monophones1 $(TMP_TRAIN_FOLDER)/phones
		cp $(TMP_TRAIN_FOLDER)/hmm_mono_silence3/* $(TMP_TRAIN_FOLDER)/hmm_final/
train_untied_triphones: tweak_silence_model
		# TODO use aligned.mlf instead of train.mlf?
		@echo -e "\n>>> make triphones from monophones\n"
		#HLEd -n $(TMP_TRAIN_FOLDER)/triphones1 -l '*' -i $(TMP_TRAIN_FOLDER)/wintri.mlf mktri.led $(TMP_TRAIN_FOLDER)/aligned.mlf
		HLEd -n $(TMP_TRAIN_FOLDER)/triphones0 -l '*' -i $(TMP_TRAIN_FOLDER)/wintri.mlf mktri.led $(TMP_TRAIN_FOLDER)/train.mlf
		cp $(TMP_TRAIN_FOLDER)/train.mlf $(TMP_TRAIN_FOLDER)/mono_train.mlf
		cp $(TMP_TRAIN_FOLDER)/wintri.mlf $(TMP_TRAIN_FOLDER)/train.mlf
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_tri_simple0
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_tri_simple1
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_tri_simple2
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_tri_simple3
		maketrihed $(TMP_TRAIN_FOLDER)/monophones0 $(TMP_TRAIN_FOLDER)/triphones0
		HHEd -B -H $(TMP_TRAIN_FOLDER)/hmm_final/macros -H $(TMP_TRAIN_FOLDER)/hmm_final/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_tri_simple0 mktri.hed $(TMP_TRAIN_FOLDER)/monophones0
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -s $(TMP_TRAIN_FOLDER)/stats -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_tri_simple0/macros -H $(TMP_TRAIN_FOLDER)/hmm_tri_simple0/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_tri_simple1 $(TMP_TRAIN_FOLDER)/triphones0 
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -s $(TMP_TRAIN_FOLDER)/stats -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_tri_simple1/macros -H $(TMP_TRAIN_FOLDER)/hmm_tri_simple1/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_tri_simple2 $(TMP_TRAIN_FOLDER)/triphones0 
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -s $(TMP_TRAIN_FOLDER)/stats -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_tri_simple2/macros -H $(TMP_TRAIN_FOLDER)/hmm_tri_simple2/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_tri_simple3 $(TMP_TRAIN_FOLDER)/triphones0 
		cp $(TMP_TRAIN_FOLDER)/hmm_tri_simple3/* $(TMP_TRAIN_FOLDER)/hmm_final/
		cp $(TMP_TRAIN_FOLDER)/triphones0 $(TMP_TRAIN_FOLDER)/phones
train_tied_triphones: train_untied_triphones
		@echo -e "\n>>> tying triphones\n"
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_tri_tied0
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_tri_tied1
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_tri_tied2
		python src/adapt_quests.py $(TMP_TRAIN_FOLDER)/monophones0 quests_example.hed $(TMP_TRAIN_FOLDER)/quests.hed
		#HDMan -n $(TMP_TRAIN_FOLDER)/fulllist -g global.ded -l flog $(TMP_TRAIN_FOLDER)/tri-dict $(TMP_TRAIN_FOLDER)/dict # this is to generate the full list of phones but we consider that we saw all triphones in the training (anyway there are the monophones)
		mkclscript TB 350.0 $(TMP_TRAIN_FOLDER)/monophones0 > $(TMP_TRAIN_FOLDER)/tb_contexts.hed
		python src/create_contexts_tying.py $(TMP_TRAIN_FOLDER)/quests.hed $(TMP_TRAIN_FOLDER)/tb_contexts.hed $(TMP_TRAIN_FOLDER)/tree.hed $(TMP_TRAIN_FOLDER)
		HHEd -B -H $(TMP_TRAIN_FOLDER)/hmm_final/macros -H $(TMP_TRAIN_FOLDER)/hmm_final/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_tri_tied0 $(TMP_TRAIN_FOLDER)/tree.hed $(TMP_TRAIN_FOLDER)/triphones0 > $(TMP_TRAIN_FOLDER)/log
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -s $(TMP_TRAIN_FOLDER)/stats -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_tri_tied0/macros -H $(TMP_TRAIN_FOLDER)/hmm_tri_tied0/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_tri_tied1 $(TMP_TRAIN_FOLDER)/tiedlist
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -s $(TMP_TRAIN_FOLDER)/stats -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_tri_tied1/macros -H $(TMP_TRAIN_FOLDER)/hmm_tri_tied1/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_tri_tied2 $(TMP_TRAIN_FOLDER)/tiedlist 
		cp $(TMP_TRAIN_FOLDER)/hmm_tri_tied2/* $(TMP_TRAIN_FOLDER)/hmm_final/
		cp $(TMP_TRAIN_FOLDER)/tiedlist $(TMP_TRAIN_FOLDER)/phones
train_mixtures:
		@echo -e "\n>>> estimating the number of mixtures\n"
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_mix0 # we will loop on these folders as we split
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_mix1
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_mix2
		mkdir -p $(TMP_TRAIN_FOLDER)/hmm_mix3
		#HERest -s $(TMP_TRAIN_FOLDER)/stats -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_final/macros -H $(TMP_TRAIN_FOLDER)/hmm_final/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix0 $(TMP_TRAIN_FOLDER)/phones 
		cp $(TMP_TRAIN_FOLDER)/hmm_final/macros $(TMP_TRAIN_FOLDER)/hmm_mix0/macros
		cp $(TMP_TRAIN_FOLDER)/hmm_final/hmmdefs $(TMP_TRAIN_FOLDER)/hmm_mix0/hmmdefs
		python src/create_mixtures_from_stats.py $(TMP_TRAIN_FOLDER)/stats
		@echo -e "\n--- mixtures of 2 components ---"
		HHEd -H $(TMP_TRAIN_FOLDER)/hmm_mix0/hmmdefs $(TMP_TRAIN_FOLDER)/TRMU2.hed $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix0/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix0/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix1 $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix1/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix1/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix2 $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix2/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix2/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix3 $(TMP_TRAIN_FOLDER)/phones
		cp $(TMP_TRAIN_FOLDER)/hmm_mix3/* $(TMP_TRAIN_FOLDER)/hmm_mix0/
		@echo -e "\n--- mixtures of 3 components ---"
		HHEd -H $(TMP_TRAIN_FOLDER)/hmm_mix0/hmmdefs $(TMP_TRAIN_FOLDER)/TRMU3.hed $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix0/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix0/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix1 $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix1/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix1/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix2 $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix2/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix2/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix3 $(TMP_TRAIN_FOLDER)/phones
		cp $(TMP_TRAIN_FOLDER)/hmm_mix3/* $(TMP_TRAIN_FOLDER)/hmm_mix0/
		@echo -e "\n--- mixtures of 5 components ---"
		HHEd -H $(TMP_TRAIN_FOLDER)/hmm_mix0/hmmdefs $(TMP_TRAIN_FOLDER)/TRMU5.hed $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix0/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix0/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix1 $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix1/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix1/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix2 $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix2/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix2/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix3 $(TMP_TRAIN_FOLDER)/phones
		cp $(TMP_TRAIN_FOLDER)/hmm_mix3/* $(TMP_TRAIN_FOLDER)/hmm_mix0/
		@echo -e "\n--- mixtures of 9 components ---"
		HHEd -H $(TMP_TRAIN_FOLDER)/hmm_mix0/hmmdefs $(TMP_TRAIN_FOLDER)/TRMU9.hed $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix0/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix0/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix1 $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix1/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix1/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix2 $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix2/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix2/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix3 $(TMP_TRAIN_FOLDER)/phones
		cp $(TMP_TRAIN_FOLDER)/hmm_mix3/* $(TMP_TRAIN_FOLDER)/hmm_mix0/
		@echo -e "\n--- mixtures of 17 components ---"
		HHEd -H $(TMP_TRAIN_FOLDER)/hmm_mix0/hmmdefs $(TMP_TRAIN_FOLDER)/TRMU17.hed $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix0/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix0/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix1 $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix1/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix1/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix2 $(TMP_TRAIN_FOLDER)/phones
		HERest -I $(TMP_TRAIN_FOLDER)/train.mlf -S $(TMP_TRAIN_FOLDER)/train.scp -H $(TMP_TRAIN_FOLDER)/hmm_mix2/macros -H $(TMP_TRAIN_FOLDER)/hmm_mix2/hmmdefs -M $(TMP_TRAIN_FOLDER)/hmm_mix3 $(TMP_TRAIN_FOLDER)/phones
		cp $(TMP_TRAIN_FOLDER)/hmm_mix3/* $(TMP_TRAIN_FOLDER)/hmm_final/
train_triphones: train_tied_triphones train_mixtures
		@echo -e "\n>>> Training fully tied triphones with GMM acoustic models\n"
train_monophones: train_monophones_monogauss tweak_silence_model train_mixtures
		@echo -e "\n>>> full training of monophones\n"
